tpp (1.3.1-8) unstable; urgency=medium

  * Rebuild against dh_elpa 2.x. (Closes: #973972)
  * Bump debhelper compatibility level to 13.
    + Build-depend on "debhelper-compat (= 13)" to replace debian/compat.
  * Declare compliance with Debian Policy 4.5.0.
  * Add lintian override for executable-in-usr-lib due to dh_elpa scripts.

 -- Axel Beckert <abe@debian.org>  Mon, 09 Nov 2020 02:58:31 +0100

tpp (1.3.1-7) unstable; urgency=medium

  * Use dh_elpa, add according build-dependency, set ELPA_NAME (see
    #905464, build-depend on dh-elpa ≥ 1.14).
    + Don't install tpp-mode.el via debian/install anymore.
  * Declare compliance with Debian Policy 4.2.1. (No changes needed.)

 -- Axel Beckert <abe@debian.org>  Sun, 28 Oct 2018 02:14:34 +0100

tpp (1.3.1-6) unstable; urgency=medium

  * Update Vcs-* headers for move to Salsa.
  * Remove trailing whitespace from ancient debian/changelog entries.
  * Fix spelling error in debian/patches/00-make.patch found by lintian.
  * Declare compliance with Debian Policy 4.2.0.
    + Update DEP5 format URL in debian/copyright to HTTPS.
  * Set "Rules-Requires-Root: no".
  * Bump debhelper compatibility level to 11.
    + Update versioned debhelper build-dependency accordingly.

 -- Axel Beckert <abe@debian.org>  Sun, 05 Aug 2018 03:17:15 +0200

tpp (1.3.1-5) unstable; urgency=medium

  * Fix typo in previous changelog entry
  * Promote figlet from suggested to recommended.
  * Suggest latex-beamer as the generated LaTeX code requires it.
  * Enhances emacsen and vim as it provides according editing modes.
  * Declare compliance with Debian Policy 3.9.7. (No changes needed.)
  * Switch Vcs-* to HTTPS and cgit webinterface.
  * Update debian/watch to no more use deprecated githubredir.debian.net.
  * Cherry-pick Rhonda's patch from
    https://github.com/akrennmair/tpp/pull/6
  * Convert debian/copyright to machine-readable DEP5 format. Add
    copyright owners for contrib/*.

 -- Axel Beckert <abe@debian.org>  Thu, 17 Mar 2016 00:02:36 +0100

tpp (1.3.1-4) unstable; urgency=medium

  * Add CVE id to 15-optional-exec.patch and previous changelog entry
  * Bump Standards-Version to 3.9.5 (no changes).
  * Add a debian/upstream/metadata file according to DEP-12.
  * Install emacs mode and vim add-on so that they can be used directly.
    + No more install them into /usr/share/doc/tpp/contrib/.
    + Suggest vim-addon-manager
    + Add README.Debian explaining how to enable the vim add-on.

 -- Axel Beckert <abe@debian.org>  Sun, 11 May 2014 18:17:39 +0200

tpp (1.3.1-3) unstable; urgency=low

  [ Jari Aalto ]
  * Switch from dpatch to source format "3.0 (quilt)". (Closes: #669595)
    + Remove dpatch traces from debian/rules and remove dpatch
      build-dependency
  * Bump debhelper compatibility to 9
    + Update versioned debhelper build-dependency
  * Use dh_prep instead of dh_clean -k
  * Fix the following lintian warnings:
    + copyright-refers-to-symlink-license
    + debhelper-but-no-misc-depends
    + debian-rules-missing-recommended-target
  * Add watch file

  [ Axel Beckert ]
  * Adopt the package (Closes: #706041)
  * Cherry-pick afb57d9 (make key events work on ruby 1.9) from upstream
    (Closes: #671530)
  * Add patch to make parsing of --exec optional (CVE-2013-2208;
    Closes: #706644)
  * Update homepage to point to GitHub
  * Fix patch header
  * Update watch file to also check release tags at GitHub
  * Revamp debian/rules:
    + No more clean up stamp files manually (dh_clean does that now)
    + Remove redundant dh_installchangelogs parameter
    + Don't compress any .tpp example file
    + Replace dh_installexamples parameter with debian/examples
    + Switch to a dh7 style debian/rules file
    + Switch to gem2deb based packaging (Closes: #671540)
      Thanks to Per Andersson!
  * Suggest texlive-latex-extra instead of transitional package texpower
  * Bump Standards-Version to 3.9.4 (no further changes necessary)
  * Recode examples to UTF-8 at build time (Closes: #705965)
    + Add build-dependency on recode
  * Add Vcs-* headers
  * Apply wrap-and-sort

 -- Axel Beckert <abe@debian.org>  Wed, 12 Jun 2013 22:18:56 +0200

tpp (1.3.1-2) unstable; urgency=low

  * Make use of the new Homepage control field instead of the tag.
  * Bump Standards-Version, no changes needed.
  * Fixing debian-rules-ignores-make-clean-error, remove make clean
    call as it is not needed.
  * Clean up copyright file, include the complete GPL preamble.
  * Removed binary-indep rule in rules cause it is not needed.

 -- Nico Golde <nion@debian.org>  Tue, 11 Dec 2007 08:22:35 +0100

tpp (1.3.1-1) unstable; urgency=low

  * New upstream release (Closes: #320302).
  * Added compat file and removed DH_COMPAT from rules.
  * Removed shelloutput patch, included upstream.
  * Added new years to copyright.

 -- Nico Golde <nion@debian.org>  Mon, 23 Apr 2007 10:59:52 +0200

tpp (1.3-2) unstable; urgency=low

  * Added patch by Gregor Herrmann to make a better
    shelloutput (Closes: #407194).
  * Changed maintainer address.
  * Bumped compat level to 5.
  * Conforms to Standards version 3.7.2.
  * Added a space in front of Homepage in control.

 -- Nico Golde <nion@debian.org>  Wed, 28 Mar 2007 10:22:29 +0200

tpp (1.3-1) unstable; urgency=low

  * new upstream release.
  * removed menu file because tpp needs an argument and it makes no sense

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- Nico Golde <nico@ngolde.de>  Thu, 23 Jun 2005 14:56:42 +0200

tpp (1.2-2) unstable; urgency=low

  * fixed typo in Suggests field.

 -- Nico Golde <nico@ngolde.de>  Sun,  6 Mar 2005 10:52:23 +0100

tpp (1.2-1) unstable; urgency=low

  * new upstream release.
  * changed arch, deleted watch file.

 -- Nico Golde <nico@ngolde.de>  Thu,  3 Mar 2005 17:05:14 +0100

tpp (1.1.1-1) unstable; urgency=low

  * new upstream release
  * added possibility of changing huge fonts (Closes: #272647)

 -- Nico Golde <nico@ngolde.de>  Wed, 13 Oct 2004 13:20:16 +0200

tpp (1.0-1) unstable; urgency=low

  * New upstream release.
  * removed debian/tpp.1 upstream manual is now available.
  * removed Co-Maintainer.
  * removed examples/ac-am.tpp from compression.
  * added Homepage to control.

 -- Nico Golde <nico@ngolde.de>  Sun, 15 Aug 2004 15:41:01 +0200

tpp (0.2-1) unstable; urgency=low

  * Initial Release (Closes: #262259).

 -- Nico Golde <nico@ngolde.de>  Sat, 31 Jul 2004 01:54:52 +0200
